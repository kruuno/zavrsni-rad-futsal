<?php
    require_once("spoj.php");
    $error = "";
    
    if(isset($_POST['dodaj_utakmicu'])){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
            $id_turnira = $_GET["id"];
            $team1 = $_POST['tim1'];
            $team2 = $_POST['tim2'];

            $sql = "SELECT * FROM zavrsnica WHERE turniri_id ='$id_turnira' AND tip = 'polufinale'";
            $result = $conn->query($sql);

            if (mysqli_num_rows($result) == 2){
                $error = "Vec je dodano 2 ekipe.";
            }else{
                $sql = "INSERT INTO zavrsnica (turniri_id, tip, team1, team2, team1_golovi, team2_golovi) VALUES ('$id_turnira', 'polufinale', '$team1', '$team2', -1, -1)";
                $conn->query($sql);
            }
        }  
    }

    if(isset($_POST['spremiRez'])){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $utakmice_id = $_POST['utakmice_id']; 
            $golovi_tim1 = $_POST['golovi_tim1'];
            $golovi_tim2 = $_POST['golovi_tim2'];

            $sql = "SELECT * FROM zavrsnica WHERE id = '$utakmice_id'";
            $result = $conn->query($sql);

            if (mysqli_num_rows($result) > 0){
                $sql = "UPDATE zavrsnica SET team1_golovi='$golovi_tim1', team2_golovi='$golovi_tim2' WHERE id = '$utakmice_id'";
                $res = $conn->query($sql);
            }
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>FUTSAL TURNIRI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/moj.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="jquery.tabledit.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body style="background-color: rgba(240, 240, 240, 0.8);">
<?php
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    include_once "navbar.php";
?>

<div style="margin-top: 5rem;" class="container">
    <div align="center" class="list-group list-group-horizontal">
        <?php 
            $id_turnira = $_GET['id'];
            $sql_korisnik=mysqli_query($conn,"SELECT korisnik_id FROM turniri WHERE id='$id_turnira'");
            $row_korisnik=mysqli_fetch_array($sql_korisnik);
            $korisnik_id = $row_korisnik['korisnik_id'];

            $result = $conn->query("SELECT * FROM grupe WHERE turniri_id = '$id_turnira' AND tip = 'playoff'");
            $row=mysqli_fetch_array($result);
            if($row['broj_ekipa'] == 2){
                echo "<a href='finale.php?id=$id_turnira' class='list-group-item list-group-item-action list-group-item-secondary'>Finale</a>";
            } else if ($row['broj_ekipa'] == 4){
                echo "<a href='polufinale.php?id=$id_turnira' class='list-group-item list-group-item-action list-group-item-secondary'>Polufinale</a>";
                echo "<a href='finale.php?id=$id_turnira' class='list-group-item list-group-item-action'>Finale</a>";
            } else if ($row['broj_ekipa'] == 8){
                echo "<a href='cetvrtfinale.php?id=$id_turnira' class='list-group-item list-group-item-action'>Četvrtfinale</a>";
                echo "<a href='polufinale.php?id=$id_turnira' class='list-group-item list-group-item-action list-group-item-secondary'>Polufinale</a>";
                echo "<a href='finale.php?id=$id_turnira' class='list-group-item list-group-item-action '>Finale</a>";
            } else if ($row['broj_ekipa'] == 16){
                echo "<a href='osminaFinala.php?id=$id_turnira' class='list-group-item list-group-item-action '>Osmina finala</a>";
                echo "<a href='cetvrtfinale.php?id=$id_turnira' class='list-group-item list-group-item-action'>Četvrtfinale</a>";
                echo "<a href='polufinale.php?id=$id_turnira' class='list-group-item list-group-item-action list-group-item-secondary'>Polufinale</a>";
                echo "<a href='finale.php?id=$id_turnira' class='list-group-item list-group-item-action '>Finale</a>";
            }
            
            ?>
    </div>
</div>

<div class="container">
    <div  class="table-responsive" style="margin-top:1rem;">
    
    <table style="background-color:white;" class="table table-striped">
    <thead>
        <tr>
        <?php if(isset($_SESSION['uloga']) && $_SESSION['uloga'] == "admin" && $_SESSION['id'] == $korisnik_id ):?>
            <th class="text-center" scope="col"></th>
        <?php endif; ?>
            <th class="text-center" scope="col">Ekipa</th>
            <th class="text-center" scope="col">Rezultat</th>
            <th class="text-center" scope="col">Ekipa</th>
        </tr>
    </thead>
    <tbody>
    <?php
        $result = $conn->query("SELECT * FROM zavrsnica WHERE turniri_id = '$id_turnira' AND tip = 'polufinale'");
        while($row=mysqli_fetch_array($result)):
        $utakmice_id = $row['id'];
    ?>
    <tr>
    <?php if(isset($_SESSION['uloga']) && $_SESSION['uloga'] == "admin" && $_SESSION['id'] == $korisnik_id ):?>
        <td scope="row">
            <button type="button" class="editButton" data-toggle="modal" data-target="#editRez<?php echo $utakmice_id;?>">
                <i class="fas fa-pencil-alt"></i>
            </button>

            <!-- Modal - POPIS EKIPA -->
            <div class="modal fade" id="editRez<?php echo $utakmice_id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h3 style="color:black;" class="modal-title" id="exampleModalLabel">Rezultat</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form name="spremiRez" class="form-signin" action="" method="POST">
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col"><p class="text-center"><?php echo $row['team1'];?></p></div>
                            <div class="col"><p class="text-center"><?php echo $row['team2'];?></p></div>
                        </div>
                        <input type="hidden" id="utakmice_id" name="utakmice_id" value="<?php echo $utakmice_id;?>">
                        <div class="row">
                            <div class="col">
                                <input name="golovi_tim1" class="text-center form-control" min="0" type="number" required>
                            </div>
                            <div class="col">
                                <input name="golovi_tim2" class="text-center form-control" min="0" type="number" required>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <input type="submit" name="spremiRez" value="Spremi rezultat" class="btn btn-secondary">
                </div>
                </form>
                </div>
            </div>
        </div>
        </td>
    <?php endif; ?>
        <td class="text-center"><?php echo $row['team1'];?></td>
        <td class="text-center"><?php if($row['team1_golovi'] == -1){
                                        echo "";
                                        }else{
                                            echo $row['team1_golovi'];
                                        }?>:<?php if($row['team2_golovi'] == -1){
                                            echo "";
                                            }else{
                                                echo $row['team2_golovi'];
                                            }?></td>
        <td class="text-center"><?php echo $row['team2'];?></td>
    </tr>
    <?php endwhile; ?>
    </tbody>
    </table>
</div>

<!-- DODAJ UTAKMICU MODAL -->
<?php 
    $sql = "SELECT * FROM zavrsnica WHERE turniri_id ='$id_turnira' AND tip = 'polufinale'";
    $result = $conn->query($sql);
    
    if (mysqli_num_rows($result) < 2 && isset($_SESSION['uloga']) && $_SESSION['uloga'] == "admin" && $_SESSION['id'] == $korisnik_id ):
?>
<div class="container">
    <div class="row">
        <div class="col">
            <button style="margin-top: 1rem; float:right; border-radius: 10px;" class="btn btn-secondary" data-toggle="modal" data-target="#dodajUtakmicu">Dodaj utakmicu</button>
        </div>
    </div>
</div>
<?php elseif(mysqli_num_rows($result) > 4): ?>
<?php endif; ?>

<!-- Modal - dodaj utakmicu u zavrsnicu -->
<div class="modal fade" id="dodajUtakmicu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
    <!-- HEADER -->
    <div class="modal-header">
        <h3 style="color:black;" class="modal-title" id="exampleModalLabel">Dodaj novu grupu</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <!-- BODY -->
    <div class="modal-body">
    <div class="container">
        <form name="dodaj_utakmicu" class="form-signin" action="" method="POST">
            
            <div class="input-group form-group">
                <select name="tim1" class="form-control" required>
                    <option value="" disabled selected>Odaberi ekipu</option>
                    <?php
                        $id_turnira = $_GET['id'];
                        $sql = mysqli_query($conn, "SELECT * FROM ekipe WHERE turniri_id = '$id_turnira'");
                        while ($row = mysqli_fetch_array($sql)){
                            echo "<option value='".$row['ime']."'>" . $row['ime'] . "</option>";
                        }
                    ?>
                </select>
            </div>
            <p class="text-center">VS</p>
            <div class="input-group form-group">
                <select name="tim2" class="form-control" required>
                    <option value="" disabled selected>Odaberi ekipu</option>
                    <?php
                        $id_turnira = $_GET['id'];
                        $sql = mysqli_query($conn, "SELECT * FROM ekipe WHERE turniri_id = '$id_turnira'");
                        while ($row = mysqli_fetch_array($sql)){
                            echo "<option value='".$row['ime']."'>" . $row['ime'] . "</option>";
                        }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <input type="submit" name="dodaj_utakmicu" value="Unesi utakmicu" class="btn-register btn-block">
            </div>
        </form>
    </div>
    </div>

</div>
</div>


<script src="main.js"></script>
</body>
</html>