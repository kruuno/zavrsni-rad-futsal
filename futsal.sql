-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2020 at 11:30 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `futsal`
--

-- --------------------------------------------------------

--
-- Table structure for table `ekipe`
--

CREATE TABLE `ekipe` (
  `id` int(11) NOT NULL,
  `turniri_id` int(11) NOT NULL,
  `grupe_id` int(11) NOT NULL,
  `ime` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `bod` int(11) NOT NULL,
  `pob` int(11) NOT NULL,
  `ner` int(11) NOT NULL,
  `izg` int(11) NOT NULL,
  `zg` int(11) NOT NULL,
  `pg` int(11) NOT NULL,
  `gr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_croatian_mysql561_ci;

--
-- Dumping data for table `ekipe`
--

INSERT INTO `ekipe` (`id`, `turniri_id`, `grupe_id`, `ime`, `bod`, `pob`, `ner`, `izg`, `zg`, `pg`, `gr`) VALUES
(1, 1, 1, 'MNK Slavija', 7, 2, 1, 0, 10, 3, 7),
(4, 1, 1, 'MNKKK', 4, 1, 1, 1, 8, 9, -1),
(5, 1, 1, 'NK Nosterija', 0, 0, 0, 3, 3, 12, -9),
(12, 1, 1, 'Futsal 1', 5, 1, 2, 0, 6, 3, 3),
(21, 1, 2, 'Dabe smo dosli', 7, 2, 1, 0, 16, 6, 10),
(22, 1, 2, 'Nova Ekipa', 0, 0, 0, 3, 3, 16, -13),
(23, 1, 2, 'FC Liverpool', 7, 2, 1, 0, 14, 7, 7),
(24, 1, 2, 'Tottenham', 3, 1, 0, 2, 5, 9, -4),
(25, 2, 0, 'MNK Slavija', 0, 0, 0, 0, 0, 0, 0),
(26, 2, 0, 'Tel Aviv', 0, 0, 0, 0, 0, 0, 0),
(27, 2, 0, 'Trabzonspor', 0, 0, 0, 0, 0, 0, 0),
(28, 2, 0, 'Tottenham', 0, 0, 0, 0, 0, 0, 0),
(29, 2, 0, 'FC Liverpool', 0, 0, 0, 0, 0, 0, 0),
(30, 3, 14, 'Tel Aviv', 0, 0, 0, 1, 5, 6, -1),
(31, 3, 14, 'FC Liverpool', 3, 1, 0, 0, 6, 5, 1),
(32, 3, 14, 'Dabe Dosli', 0, 0, 0, 0, 0, 0, 0),
(33, 3, 14, 'DC', 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `grupe`
--

CREATE TABLE `grupe` (
  `id` int(11) NOT NULL,
  `turniri_id` int(11) NOT NULL,
  `ime` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `tip` varchar(50) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `broj_ekipa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_croatian_mysql561_ci;

--
-- Dumping data for table `grupe`
--

INSERT INTO `grupe` (`id`, `turniri_id`, `ime`, `tip`, `broj_ekipa`) VALUES
(1, 1, 'Grupa A', 'grupa', 4),
(2, 1, 'Grupa B', 'grupa', 4),
(3, 1, 'Grupa C', 'grupa', 4),
(4, 1, 'Grupa D', 'grupa', 4),
(12, 1, 'Play-Off', 'playoff', 8),
(13, 2, 'Playoff New', 'playoff', 4),
(14, 3, 'Grupa A', 'grupa', 4);

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `id` int(11) NOT NULL,
  `k_ime` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `ime` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `prezime` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `email` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `lozinka` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `uloga` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_croatian_mysql561_ci;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`id`, `k_ime`, `ime`, `prezime`, `email`, `lozinka`, `uloga`) VALUES
(4, 'kruuno', 'Kruno', 'Kovac', 'kkovac@ferit.hr', '123', 'korisnik'),
(10, 'kkovac1', 'Krunoslav', 'Kovac', 'kkovac1@etfos.hr', '123', 'admin'),
(11, 'prijava', 'Ja', 'Sam', 'prijava@gmail.com', 'prijava', 'korisnik'),
(12, 'test', 'Test', 'Testiram', 'test@mail.com', 'test', 'admin'),
(13, 'enav', 'Ena', 'Vrdoljak', 'enav@mail.com', '3f265375ee4fb99f2439f3759683b34f', 'admin'),
(14, 'krunosa', 'Kruno', 'Vacko', 'krunosa@mail.com', '202cb962ac59075b964b07152d234b70', 'korisnik'),
(15, 'nokru', 'Kruno', 'Kova', 'nokru@gmail.com', '202cb962ac59075b964b07152d234b70', 'admin'),
(16, 'shady', 'Slim', 'Shady', 'shady@gmail.com', '202cb962ac59075b964b07152d234b70', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `rezultat`
--

CREATE TABLE `rezultat` (
  `id` int(11) NOT NULL,
  `utakmica_id` int(11) NOT NULL,
  `golovi_tim1` int(11) NOT NULL,
  `golovi_tim2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_croatian_mysql561_ci;

--
-- Dumping data for table `rezultat`
--

INSERT INTO `rezultat` (`id`, `utakmica_id`, `golovi_tim1`, `golovi_tim2`) VALUES
(10, 1, 4, 2),
(11, 2, 0, 3),
(12, 3, 5, 0),
(13, 4, 2, 2),
(14, 5, 1, 1),
(15, 6, 4, 3),
(16, 25, 7, 0),
(17, 26, 3, 1),
(18, 27, 5, 5),
(19, 28, 2, 3),
(20, 29, 4, 1),
(21, 30, 1, 6),
(25, 137, 4, 3),
(26, 138, 1, 1),
(27, 148, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `turniri`
--

CREATE TABLE `turniri` (
  `id` int(11) NOT NULL,
  `korisnik_id` int(11) NOT NULL,
  `ime` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `tip` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `kotizacija` int(11) NOT NULL,
  `datum_pocetka` date NOT NULL,
  `lokacija` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `nagradniFond` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_croatian_mysql561_ci;

--
-- Dumping data for table `turniri`
--

INSERT INTO `turniri` (`id`, `korisnik_id`, `ime`, `tip`, `kotizacija`, `datum_pocetka`, `lokacija`, `nagradniFond`) VALUES
(1, 13, 'Slavija Cup', 'kup', 200, '2019-07-12', 'Vinkovci', 5000),
(2, 16, 'New One', 'knockout', 333, '2019-09-18', 'Vinkovci', 5555),
(3, 16, 'New One Kup Type', 'kup', 444, '2019-09-20', 'Vinkovci', 7777);

-- --------------------------------------------------------

--
-- Table structure for table `utakmica`
--

CREATE TABLE `utakmica` (
  `id` int(11) NOT NULL,
  `team1_id` int(11) NOT NULL,
  `team2_id` int(11) NOT NULL,
  `grupe_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_croatian_mysql561_ci;

--
-- Dumping data for table `utakmica`
--

INSERT INTO `utakmica` (`id`, `team1_id`, `team2_id`, `grupe_id`) VALUES
(1, 1, 4, 1),
(2, 5, 12, 1),
(3, 1, 5, 1),
(4, 4, 12, 1),
(5, 1, 12, 1),
(6, 4, 5, 1),
(25, 21, 22, 2),
(26, 23, 24, 2),
(27, 21, 23, 2),
(28, 22, 24, 2),
(29, 21, 24, 2),
(30, 22, 23, 2),
(67, 6, 25, 5),
(68, 26, 27, 5),
(69, 26, 6, 5),
(70, 25, 27, 5),
(71, 6, 27, 5),
(72, 25, 26, 5),
(73, 19, 31, 6),
(74, 32, 33, 6),
(75, 32, 19, 6),
(76, 31, 33, 6),
(77, 19, 33, 6),
(78, 31, 32, 6),
(115, 36, 37, 9),
(116, 37, 35, 9),
(117, 37, 34, 9),
(118, 35, 36, 9),
(119, 36, 34, 9),
(120, 34, 35, 9),
(136, 49, 50, 23),
(137, 50, 48, 23),
(138, 50, 47, 23),
(139, 48, 49, 23),
(140, 49, 47, 23),
(141, 47, 48, 23),
(148, 30, 31, 14),
(149, 32, 33, 14),
(150, 32, 30, 14),
(151, 31, 33, 14),
(152, 30, 33, 14),
(153, 31, 32, 14);

-- --------------------------------------------------------

--
-- Table structure for table `zavrsnica`
--

CREATE TABLE `zavrsnica` (
  `id` int(11) NOT NULL,
  `turniri_id` int(11) NOT NULL,
  `tip` varchar(50) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `team1` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `team2` varchar(100) COLLATE utf32_croatian_mysql561_ci NOT NULL,
  `team1_golovi` int(11) NOT NULL,
  `team2_golovi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_croatian_mysql561_ci;

--
-- Dumping data for table `zavrsnica`
--

INSERT INTO `zavrsnica` (`id`, `turniri_id`, `tip`, `team1`, `team2`, `team1_golovi`, `team2_golovi`) VALUES
(1, 1, 'cetvrtfinale', 'MNK Slavija', 'FC Liverpool', 3, 2),
(3, 1, 'cetvrtfinale', 'Futsal 1', 'Dabe smo dosli', 2, 3),
(4, 1, 'cetvrtfinale', 'Nova Ekipa', 'Tottenham', 1, 4),
(5, 1, 'cetvrtfinale', 'MNKKK', 'NK Nosterija', 5, 0),
(6, 1, 'polufinale', 'MNK Slavija', 'Dabe smo dosli', 4, 3),
(7, 1, 'polufinale', 'Tottenham', 'MNKKK', 3, 1),
(8, 1, 'finale', 'MNK Slavija', 'Tottenham', 5, 4),
(9, 2, 'polufinale', 'MNK Slavija', 'Tel Aviv', -1, -1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ekipe`
--
ALTER TABLE `ekipe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ekipe_ibfk_1` (`turniri_id`);

--
-- Indexes for table `grupe`
--
ALTER TABLE `grupe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `turniri_id` (`turniri_id`);

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rezultat`
--
ALTER TABLE `rezultat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utakmica_id` (`utakmica_id`);

--
-- Indexes for table `turniri`
--
ALTER TABLE `turniri`
  ADD PRIMARY KEY (`id`),
  ADD KEY `korisnik_id` (`korisnik_id`);

--
-- Indexes for table `utakmica`
--
ALTER TABLE `utakmica`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zavrsnica`
--
ALTER TABLE `zavrsnica`
  ADD PRIMARY KEY (`id`),
  ADD KEY `turniri_id` (`turniri_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ekipe`
--
ALTER TABLE `ekipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `grupe`
--
ALTER TABLE `grupe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `korisnici`
--
ALTER TABLE `korisnici`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `rezultat`
--
ALTER TABLE `rezultat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `turniri`
--
ALTER TABLE `turniri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `utakmica`
--
ALTER TABLE `utakmica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `zavrsnica`
--
ALTER TABLE `zavrsnica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ekipe`
--
ALTER TABLE `ekipe`
  ADD CONSTRAINT `ekipe_ibfk_1` FOREIGN KEY (`turniri_id`) REFERENCES `turniri` (`id`);

--
-- Constraints for table `grupe`
--
ALTER TABLE `grupe`
  ADD CONSTRAINT `grupe_ibfk_1` FOREIGN KEY (`turniri_id`) REFERENCES `turniri` (`id`);

--
-- Constraints for table `rezultat`
--
ALTER TABLE `rezultat`
  ADD CONSTRAINT `rezultat_ibfk_1` FOREIGN KEY (`utakmica_id`) REFERENCES `utakmica` (`id`);

--
-- Constraints for table `turniri`
--
ALTER TABLE `turniri`
  ADD CONSTRAINT `turniri_ibfk_1` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnici` (`id`);

--
-- Constraints for table `zavrsnica`
--
ALTER TABLE `zavrsnica`
  ADD CONSTRAINT `zavrsnica_ibfk_1` FOREIGN KEY (`turniri_id`) REFERENCES `turniri` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
