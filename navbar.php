<nav class="navbar navbar-expand-sm my-nav navbar-dark fixed-top">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="navbar-brand" href="index.php">Početna</a>
            </li>
        </ul>
        <button type="button" class="navbar-toggler"  data-toggle="collapse" data-target="#myNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="Turniri.php">Turniri</a>
            </li>
            
        </ul>
        </div>

        <div class="collapse navbar-collapse justify-content-end" id="myNavbar">
                <?php if(isset($_SESSION['k_ime'])):?>
                    <?php if(isset($_SESSION['k_ime']) && $_SESSION['uloga'] == "admin" ):?>
                        <ul class="navbar-nav">
                            <li><a class="nav-link" href="mojiTurniri.php"><?php echo $_SESSION['ime'] ?></a></li>
                        </ul>
                    <?php endif; ?>
                    <?php if(isset($_SESSION['k_ime']) && $_SESSION['uloga'] != "admin" ):?>
                        <ul class="navbar-nav">
                            <li><a class="nav-link"><?php echo $_SESSION['ime'] ?></a></li>
                        </ul>
                    <?php endif; ?>
                <ul class="navbar-nav">
                    <li><a class="nav-link" href="odjava.php"><i class="fas fa-sign-out-alt"></i> Odjavi se</a></li>
                </ul>
                <?php else: ?>
                <ul class="navbar-nav">
                    <li><a class="nav-link" href="prijava.php"><i class="fas fa-sign-in-alt"></i> Prijava</a></li>
                </ul>
                <ul class="navbar-nav">
                    <li><a class="nav-link" href="registracija.php"><i class="fas fa-user-plus"></i> Registracija</a> </li>
                </ul>
                <?php endif; ?>
        </div>
</nav>