<?php 
    require_once("spoj.php");
    $error = "";

    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    if(isset($_POST['spremiRez'])){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $utakmice_id = $_POST['utakmice_id']; 
            $sql = "SELECT * FROM rezultat WHERE utakmica_id = '$utakmice_id'";
            $old_result = $conn->query($sql);
            $row=mysqli_fetch_array($old_result);
            $stari_goloviTim1 = $row['golovi_tim1'];
            $stari_goloviTim2 = $row['golovi_tim2'];

            $golovi_tim1 = $_POST['golovi_tim1'];
            $golovi_tim2 = $_POST['golovi_tim2'];
            

            $sql = "SELECT * FROM rezultat WHERE utakmica_id = '$utakmice_id'";
            $result = $conn->query($sql);
            

            if (mysqli_num_rows($result) > 0){
                $sql = "UPDATE rezultat SET golovi_tim1='$golovi_tim1', golovi_tim2='$golovi_tim2' WHERE utakmica_id = '$utakmice_id'";
                $res = $conn->query($sql);

                if(($golovi_tim1 > $golovi_tim2 && $stari_goloviTim1 > $stari_goloviTim2) || ($golovi_tim1 < $golovi_tim2 && $stari_goloviTim1 < $stari_goloviTim2) || ($golovi_tim1 == $golovi_tim2 && $stari_goloviTim1 == $stari_goloviTim2)){ 
                    $razlika_tim1 = $stari_goloviTim1 - $golovi_tim1;                      // 1. SLUCAJ AKO SU SE SAMO PROMIJENILI GOLOVI EKIPA
                    $razlika_tim2 = $stari_goloviTim2 - $golovi_tim2;
                    $sq = "UPDATE ekipe
                        SET zg=zg-'$razlika_tim1', pg=pg-'$razlika_tim2', gr=zg-pg
                        WHERE id = (SELECT team1_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);

                    $sq = "UPDATE ekipe
                        SET zg=zg-'$razlika_tim2', pg=pg-'$razlika_tim1', gr=zg-pg
                        WHERE id = (SELECT team2_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);
                }else if($golovi_tim1 < $golovi_tim2 && $stari_goloviTim1 > $stari_goloviTim2){ //2. SLUCAJ - Ako je TIM 1 bio pobjednik, a sada je gubitnik
                    $razlika_tim1 = $stari_goloviTim1 - $golovi_tim1;
                    $razlika_tim2 = $stari_goloviTim2 - $golovi_tim2;
                    $sq = "UPDATE ekipe
                        SET bod=bod-3, pob=pob-1, izg=izg+1, zg=zg-'$razlika_tim1', pg=pg-'$razlika_tim2', gr=zg-pg
                        WHERE id = (SELECT team1_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);

                    $sq = "UPDATE ekipe
                        SET bod=bod+3, pob=pob+1, izg=izg-1, zg=zg-'$razlika_tim2', pg=pg-'$razlika_tim1', gr=zg-pg
                        WHERE id = (SELECT team2_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);
                }else if($golovi_tim1 > $golovi_tim2 && $stari_goloviTim1 < $stari_goloviTim2){ //3. SLUCAJ - Ako je TIM 1 bio gubitnik, a sada je pobjednik
                    $razlika_tim1 = $stari_goloviTim1 - $golovi_tim1;
                    $razlika_tim2 = $stari_goloviTim2 - $golovi_tim2;
                    $sq = "UPDATE ekipe
                        SET bod=bod+3, pob=pob+1, izg=izg-1, zg=zg-'$razlika_tim1', pg=pg-'$razlika_tim2', gr=zg-pg
                        WHERE id = (SELECT team1_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);

                    $sq = "UPDATE ekipe
                        SET bod=bod-3, pob=pob-1, izg=izg+1, zg=zg-'$razlika_tim2', pg=pg-'$razlika_tim1', gr=zg-pg
                        WHERE id = (SELECT team2_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);
                }else if($golovi_tim1 == $golovi_tim2 && $stari_goloviTim1 > $stari_goloviTim2){ //4. SLUCAJ - Ako je TIM 1 bio pobjednik,
                    $razlika_tim1 = $stari_goloviTim1 - $golovi_tim1;                            //a sada je neriješno
                    $razlika_tim2 = $stari_goloviTim2 - $golovi_tim2;
                    $sq = "UPDATE ekipe
                        SET bod=bod-2, pob=pob-1, ner=ner+1, zg=zg-'$razlika_tim1', pg=pg-'$razlika_tim2', gr=zg-pg
                        WHERE id = (SELECT team1_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);

                    $sq = "UPDATE ekipe
                        SET bod=bod+1, ner=ner+1, izg=izg-1, zg=zg-'$razlika_tim2', pg=pg-'$razlika_tim1', gr=zg-pg
                        WHERE id = (SELECT team2_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);
                }else if($golovi_tim1 == $golovi_tim2 && $stari_goloviTim1 < $stari_goloviTim2){ //5. SLUCAJ - Ako je TIM 1 bio gubitnik,
                    $razlika_tim1 = $stari_goloviTim1 - $golovi_tim1;                            //a sada je neriješno
                    $razlika_tim2 = $stari_goloviTim2 - $golovi_tim2;
                    $sq = "UPDATE ekipe
                        SET bod=bod+1, izg=izg-1, ner=ner+1, zg=zg-'$razlika_tim1', pg=pg-'$razlika_tim2', gr=zg-pg
                        WHERE id = (SELECT team1_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);

                    $sq = "UPDATE ekipe
                        SET bod=bod-2, pob=pob-1, ner=ner+1, zg=zg-'$razlika_tim2', pg=pg-'$razlika_tim1', gr=zg-pg
                        WHERE id = (SELECT team2_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);
                }else if($golovi_tim1 > $golovi_tim2 && $stari_goloviTim1 == $stari_goloviTim2){ //6. SLUCAJ - Ako je bilo nerijeseno,
                    $razlika_tim1 = $stari_goloviTim1 - $golovi_tim1;                            //a sada je tim 1 pobjednik
                    $razlika_tim2 = $stari_goloviTim2 - $golovi_tim2;
                    $sq = "UPDATE ekipe
                        SET bod=bod+2, pob=pob+1, ner=ner-1, zg=zg-'$razlika_tim1', pg=pg-'$razlika_tim2', gr=zg-pg
                        WHERE id = (SELECT team1_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);

                    $sq = "UPDATE ekipe
                        SET bod=bod-1, ner=ner-1, izg=izg+1, zg=zg-'$razlika_tim2', pg=pg-'$razlika_tim1', gr=zg-pg
                        WHERE id = (SELECT team2_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);
                }else if($golovi_tim1 < $golovi_tim2 && $stari_goloviTim1 == $stari_goloviTim2){ //6. SLUCAJ - Ako je bilo nerijeseno,
                    $razlika_tim1 = $stari_goloviTim1 - $golovi_tim1;                            //a sada je tim 1 gubitnik
                    $razlika_tim2 = $stari_goloviTim2 - $golovi_tim2;
                    $sq = "UPDATE ekipe
                        SET bod=bod-1, ner=ner-1, izg=izg+1, zg=zg-'$razlika_tim1', pg=pg-'$razlika_tim2', gr=zg-pg
                        WHERE id = (SELECT team1_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);

                    $sq = "UPDATE ekipe
                        SET bod=bod+2, pob=pob+1, ner=ner-1, zg=zg-'$razlika_tim2', pg=pg-'$razlika_tim1', gr=zg-pg
                        WHERE id = (SELECT team2_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);
                }

            } else {
                $result = $conn->query("INSERT INTO rezultat (utakmica_id, golovi_tim1, golovi_tim2) VALUES ('$utakmice_id','$golovi_tim1','$golovi_tim2')");
                if($golovi_tim1 > $golovi_tim2){
                    $sq = "UPDATE ekipe
                        SET bod=bod+3, pob=pob+1, zg=zg+'$golovi_tim1', pg=pg+'$golovi_tim2', gr=zg-pg
                        WHERE id = (SELECT team1_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);

                    $sq = "UPDATE ekipe
                        SET izg=izg+1, zg=zg+'$golovi_tim2', pg=pg+'$golovi_tim1', gr=zg-pg
                        WHERE id = (SELECT team2_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);

                }else if($golovi_tim1 < $golovi_tim2){
                    $sq = "UPDATE ekipe
                        SET izg=izg+1, zg=zg+'$golovi_tim1', pg=pg+'$golovi_tim2', gr=zg-pg
                        WHERE id = (SELECT team1_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);

                    $sq = "UPDATE ekipe
                        SET bod=bod+3, pob=pob+1, zg=zg+'$golovi_tim2', pg=pg+'$golovi_tim1', gr=zg-pg
                        WHERE id = (SELECT team2_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);
                }else{
                    $sq = "UPDATE ekipe
                        SET bod=bod+1, ner=ner+1, zg=zg+'$golovi_tim1', pg=pg+'$golovi_tim2', gr=zg-pg
                        WHERE id = (SELECT team1_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);

                    $sq = "UPDATE ekipe
                        SET bod=bod+1, ner=ner+1, zg=zg+'$golovi_tim2', pg=pg+'$golovi_tim1', gr=zg-pg
                        WHERE id = (SELECT team2_id FROM utakmica WHERE id = '$utakmice_id')";
                    $ekipe = $conn->query($sq);
                }
            }
        }
    }  
?>

<!DOCTYPE html>
<html>
<head>
    <title>FUTSAL TURNIRI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/moj.css">
    <meta charset="UTF-8">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="jquery.tabledit.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body style="background-color: rgba(240, 240, 240, 0.8);">
<?php
    session_start();
    include_once "navbar.php";
?>

<div style="margin-top: 5rem;" class="container">
    <div align="center" class="list-group list-group-horizontal">
        <?php $id_turnira = $_GET['id'];
            $sql_korisnik=mysqli_query($conn,"SELECT korisnik_id FROM turniri WHERE id='$id_turnira'");
            $row_korisnik=mysqli_fetch_array($sql_korisnik);
            $korisnik_id = $row_korisnik['korisnik_id'];
        ?>
        <a href="poredak.php?id=<?php echo $id_turnira; ?>" class="list-group-item list-group-item-action">Poredak</a>
        <a href="rezultati.php?id=<?php echo $id_turnira; ?>" class="list-group-item list-group-item-action list-group-item-secondary">Rezultati</a>
    </div>
</div>

<div style="margin-top: 1rem;" class="container">
    <div align="center" class="list-group list-group-horizontal" id="myDIV">
        <?php
            $id_turnira = $_GET["id"];
            $query = "SELECT * FROM grupe WHERE turniri_id ='$id_turnira' AND tip = 'grupa'";
            $result=mysqli_query($conn,$query);
            
            while($row=mysqli_fetch_array($result)):
        ?>
            <a href="rezultati.php?id=<?php echo $id_turnira;?>&id_grupe=<?php echo $row['id'];?>" class="list-group-item list-group-item-action"><?php echo $row['ime']?></a>
        <?php endwhile;?>
    </div>
</div>

<div class="container">
    <div  class="table-responsive" style="margin-top:1rem;">
        <table style="background-color:white;" class="table table-striped">
        <thead>
            <tr>
            <?php if(isset($_SESSION['uloga']) && $_SESSION['uloga'] == "admin" && $_SESSION['id'] == $korisnik_id ):?>
                <th class="text-center" scope="col"></th>
            <?php endif; ?>
                <th class="text-center" scope="col">Ekipa</th>
                <th class="text-center" scope="col">Rezultat</th>
                <th class="text-center" scope="col">Ekipa</th>
            </tr>
        </thead>
        <tbody>

        <?php
            $id_turnira = $_GET["id"];

            if(isset($_GET['id_grupe'])){
                $id_grupe = $_GET['id_grupe'];
            }
            
            if(empty($id_grupe)){
                $res=mysqli_query($conn, "SELECT id FROM grupe WHERE turniri_id='$id_turnira'");
                $row=mysqli_fetch_array($res);
                $id_grupe = $row['id'];
            }

            $query = "SELECT * FROM utakmica WHERE grupe_id ='$id_grupe'";
            $result=mysqli_query($conn,$query);
            
            
            while($row=mysqli_fetch_array($result)):
                $tim1_id = $row['team1_id'];
                $tim2_id = $row['team2_id'];
                $utakmice_id = $row['id'];
                $imena=mysqli_query($conn,"SELECT ime FROM ekipe WHERE id IN ($tim1_id, $tim2_id) ORDER BY FIELD(id, $tim1_id, $tim2_id)");
                $red=mysqli_fetch_array($imena);
                
                $ime = array();
                foreach($imena as $row) {
                    $ime[]=$row['ime'];
                }
                
        ?>
        <tr>
        
        <?php if(isset($_SESSION['uloga']) && $_SESSION['uloga'] == "admin" && $_SESSION['id'] == $korisnik_id ):?>
            <td scope="row">
                <button type="button" class="editButton" data-toggle="modal" data-target="#editRez<?php echo $utakmice_id;?>">
                    <i class="fas fa-pencil-alt"></i>
                </button>

                <!-- Modal - POPIS EKIPA -->
                <div class="modal fade" id="editRez<?php echo $utakmice_id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h3 style="color:black;" class="modal-title" id="exampleModalLabel">Rezultat</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form name="spremiRez" class="form-signin" action="" method="POST">
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="col"><p class="text-center"><?php echo $ime[0];?></p></div>
                                <div class="col"><p class="text-center"><?php echo $ime[1];?></p></div>
                            </div>
                            <input type="hidden" id="utakmice_id" name="utakmice_id" value="<?php echo $utakmice_id;?>">
                            <div class="row">
                                <div class="col">
                                    <input name="golovi_tim1" class="text-center form-control" min="0" type="number" required>
                                </div>
                                <div class="col">
                                    <input name="golovi_tim2" class="text-center form-control" min="0" type="number" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <input type="submit" name="spremiRez" value="Spremi rezultat" class="btn btn-secondary">
                    </div>
                    </form>
                    </div>
                </div>
            </div>
            </td>
        <?php endif; ?>
            <td class="text-center" ><?php echo $ime[0]; ?></td>
            <?php 
                $sql = "SELECT * FROM rezultat WHERE utakmica_id = '$utakmice_id'";
                $rezultat_utakmice = $conn->query($sql);
                $red=mysqli_fetch_array($rezultat_utakmice);
            ?>
            <td class="text-center"><?php echo $red['golovi_tim1'];?>:<?php echo $red['golovi_tim2'];?></td>
            <td class="text-center"><?php echo $ime[1];?></td>
        </tr>
        <?php endwhile;?>
        </tbody>
        </table>
    </div>
    <?php

        $check = "SELECT * FROM utakmica WHERE grupe_id = '$id_grupe'";
        $result_check = $conn->query($check);
        echo $result_check->num_rows;
        if(isset($_SESSION['uloga']) && $_SESSION['uloga'] == "admin" && $_SESSION['id'] == $korisnik_id){
            if($result_check->num_rows > 0){
                echo "<button class='btn btn-secondary' disabled>Raspored je generiran</button>";
            } else {
            echo "<a class='btn btn-secondary' href='generirajUtakmice.php?id_grupe=$id_grupe&id=$id_turnira'>Generiraj raspored</a>";
            }
        }
    ?>
    

</div>

</body>
</html>