<?php
    require_once("spoj.php");
    session_start();
    $error = "";
    
    if(isset($_POST['dodajEkipu'])){
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $ime = $_POST['ekipe'];
        $id_grupe = $_GET['id_grupe'];
        $turnir_id = $_GET['id'];
        
        $count = $conn->query("SELECT COUNT('id') FROM ekipe WHERE grupe_id ='$id_grupe' AND turniri_id = '$turnir_id'");
        $row=mysqli_fetch_array($count);
        $broj_ekipa = $row[0];

        $sql = "SELECT * FROM ekipe WHERE ime = '$ime' AND turniri_id = '$turnir_id'";
        $result = $conn->query($sql);
        $row=mysqli_fetch_array($result);
        $id_ekipe = $row['id'];
        $check = $row['grupe_id'];

        if ($check != 0 || $broj_ekipa == 4){
            $error = "Ekipa je već dodana u neku grupu ili grupa sadrži maksimalan broj ekipa.";
        }else{
            $sql = "UPDATE ekipe SET grupe_id='$id_grupe' WHERE id = '$id_ekipe'";
            
            if ($conn->query($sql) == TRUE){	
                    echo "<script type='text/javascript'>alert('Uspješno ste dodali ekipu!');</script>";
				}else{
				echo $conn->error;
			}
        }
    }  
} 
?>

<!DOCTYPE html>
<html>
<head>
    <title>FUTSAL TURNIRI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/moj.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body style="background-color: rgba(240, 240, 240, 0.8);">
<?php
    include_once "navbar.php";
?>

<div class="container-fluid">
    <div class="d-flex justify-content-center">
        <div class="card">
            <div class="card-header" style="background-color: white;">
                <h3>Dodaj ekipu</h3>
            </div>
            <div class="card-body">
                <form name="dodajEkipu" class="form-signin" action="" method="POST">
                    
                <div class="input-group form-group">
                        <select name="ekipe" class="form-control" required>
                            <option value="" disabled selected>Odaberi ekipu</option>
                            <?php
                                $id_turnira = $_GET['id'];
                                $sql = mysqli_query($conn, "SELECT * FROM ekipe WHERE turniri_id = '$id_turnira'");
                                while ($row = mysqli_fetch_array($sql)){
                                    echo "<option value='".$row['ime']."'>" . $row['ime'] . "</option>";
                                }
                            ?>
                        </select>
                    </div>

                    <div> 
                        <?php if($error != "") {
                            echo "<p>$error</p>";
                            }
                        ?>  
                    </div>

                    <div class="form-group">
                        <input type="submit" name="dodajEkipu" value="Dodaj" class="btn-register btn-block">
                    </div>
                </form>
            </div>

            <a id="a-footer" class="" href="poredak.php?id=<?php echo $_GET['id'] ?>">
            <div class="card-footer">
                <p>Povratak na pregled poretka</p>
            </div>
            </a>
            
        </div>
    </div>
</div>

</body>
</html>
