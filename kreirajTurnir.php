<?php
    require_once("spoj.php");
    session_start();
    $error = "";
    
    if(isset($_POST['kreiraj'])){
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $ime = $_POST['ime'];
        $tip = $_POST['tip'];
        $kotizacija = $_POST['kotizacija'];
        $datum_pocetka = $_POST['datumPocetka'];
        $lokacija = $_POST['lokacija'];
        $fond = $_POST['nagradniFond'];


        $sql = "SELECT * FROM turniri WHERE ime = '$ime'";
        $result = $conn->query($sql);

        if (mysqli_num_rows($result) > 0){
            $error = "Ime turnira je zauzeto.";
        }else{
            $korisnik_id = $_SESSION['id'];
            $sql = "INSERT INTO turniri (korisnik_id, ime, tip, kotizacija, datum_pocetka, lokacija, nagradniFond) 
                                VALUES ('$korisnik_id','$ime', '$tip', '$kotizacija', '$datum_pocetka', '$lokacija', '$fond')";
            if ($conn->query($sql) == TRUE){	
                    echo "<script type='text/javascript'>alert('Uspješno ste kreirali turnir!');</script>";
				}else{
				echo $conn->error;
			}
        }
    }  
} 
?>

<!DOCTYPE html>
<html>
<head>
    <title>FUTSAL TURNIRI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/moj.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body style="background-color: rgba(240, 240, 240, 0.8);">
<?php
    include_once "navbar.php";
?>

<div class="container-fluid">
    <div class="d-flex justify-content-center">
        <div class="card">
            <div class="card-header" style="background-color: white;">
                <h3>Kreiraj Turnir</h3>
            </div>
            <div class="card-body">
                <form name="kreiraj" class="form-signin" action="" method="POST">
                    
                    <div class="input-group form-group">
                        <input type="text" name="ime" class="form-control" placeholder="Unesite ime turnira" autofocus required>
                    </div>

                    <div class="input-group form-group">
                        <select name="tip" class="form-control" required>
                            <option value="" disabled selected>Odaberi tip turnira</option>
                            <option value="kup">Kup</option>
                            <option value="knockout">Knockout</option>
                        </select>
                    </div>

                    <div class="input-group form-group">
                        <input type="number" min="1" name="kotizacija" class="form-control" placeholder="Kotizacija (kn)" required>
                    </div>

                    <div class="input-group form-group">
                        <input type="date" name="datumPocetka" class="form-control" required>
                    </div>

                    <div class="input-group form-group">
                        <input type="text" name="lokacija" class="form-control" placeholder="Lokacija (ime grada/sela)" required>
                    </div>

                    <div class="input-group form-group">
                        <input type="number" min="1" name="nagradniFond" class="form-control" placeholder="Nagradni fond (kn)" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="kreiraj" value="Kreiraj" class="btn-register btn-block">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<script src="main.js"></script>
</body>
</html>
