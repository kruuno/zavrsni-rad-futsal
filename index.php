<!DOCTYPE html>
<html>
<head>
    <title>FUTSAL TURNIRI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/moj.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body>
<?php
    session_start();
    include_once "navbar.php";
?>

<div id="i1" class="home-bg-fixed home-slide-centered">
    <div class="container padd-all">
        <h1>
            Voliš nogomet i Tražiš turnir?
            <hr>
            <p>Dobro Došao!</p>
            <hr>
            
        </h1>
        <br>
        <?php if(isset($_SESSION['k_ime'])):?>
            <a class="btn btn-lg" href="Turniri.php">Prijavi svoju ekipu</a>
        <?php else: ?>
            <a class="btn btn-lg" href="registracija.php">Prijavi svoju ekipu</a>
        <?php endif; ?>
    </div>
</div>

</body>
</html>