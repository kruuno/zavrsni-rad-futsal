<?php
    require_once("spoj.php");
    $error = "";
    
    if(isset($_POST['dodaj_grupu'])){
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $id_turnira = $_GET["id"];
        $ime = $_POST['ime'];
        $broj_ekipa = $_POST['broj_ekipa'];

        $sql = "SELECT * FROM grupe WHERE ime = '$ime' AND turniri_id ='$id_turnira'";
        $result = $conn->query($sql);

        if (mysqli_num_rows($result) > 0){
            $error = "Ime grupe vec postoji.";
        }else{
            $sql = "INSERT INTO grupe (ime, turniri_id, tip, broj_ekipa) VALUES ('$ime', '$id_turnira', 'grupa', '$broj_ekipa')";
            if ($conn->query($sql) == TRUE){	
                    echo "<script type='text/javascript'>alert('Uspješno ste kreirali grupu!');</script>";
				}else{
				echo $conn->error;
			}
        }
    }  
} 
?>

<!DOCTYPE html>
<html>
<head>
    <title>FUTSAL TURNIRI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/moj.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="jquery.tabledit.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body style="background-color: rgba(240, 240, 240, 0.8);">
<?php
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    include_once "navbar.php";
?>

<div style="margin-top: 5rem;" class="container">
    <div align="center" class="list-group list-group-horizontal">
        <?php $id_turnira = $_GET['id'];?>
        <a href="poredak.php?id=<?php echo $id_turnira; ?>" class="list-group-item list-group-item-action list-group-item-secondary">Poredak</a>
        <a href="rezultati.php?id=<?php echo $id_turnira; ?>" class="list-group-item list-group-item-action">Rezultati</a>
    </div>
</div>
      
<div style="margin-top: 1rem;" class="container">
    <div align="center" class="list-group list-group-horizontal">
        <?php
            $id_turnira = $_GET["id"];
            $rezultat=mysqli_query($conn,"SELECT * FROM turniri WHERE id = '$id_turnira'");
            $get_tip_turnira = mysqli_fetch_array($rezultat);
            $tip_turnira = $get_tip_turnira['tip'];
            if($tip_turnira == "knockout"){ // AKO JE TIP TURNIRA knockout odmah prebaci na zavrsnicu
                header('Location: zavrsnica.php?id='.$id_turnira);
            }

            $query = "SELECT * FROM grupe WHERE turniri_id ='$id_turnira' AND tip = 'grupa'";
            $result=mysqli_query($conn,$query);
            
            while($row=mysqli_fetch_array($result)):
        ?>
        <a href="poredak.php?id=<?php echo $id_turnira;?>&id_grupe=<?php echo $row['id'];?>" class="list-group-item list-group-item-action "><?php echo $row['ime']?></a>
        
        <?php endwhile;?>
    </div>
</div>
<?php 
    $id_turnira = $_GET["id"];
    $query = "SELECT * FROM grupe WHERE turniri_id ='$id_turnira' AND tip = 'grupa'";
    $result=mysqli_query($conn,$query);
    $row=mysqli_fetch_array($result);
    if($row['turniri_id'] == $id_turnira):
?>
<div class="container">
        <div  class="table-responsive" style="margin-top:1rem;">
        
        <table style="background-color:white;" class="table table-striped">
        <thead>
            <tr>
                <th scope="col">IME</th>
                <th scope="col">BOD</th>
                <th scope="col">POB</th>
                <th scope="col">NER</th>
                <th scope="col">IZG</th>
                <th scope="col">ZG</th>
                <th scope="col">PG</th>
                <th scope="col">GR</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $id_turnira = $_GET["id"];
                if(isset($_GET['id_grupe'])){
                    $id_grupe = $_GET['id_grupe'];
                }
                
                if(empty($id_grupe)){
                    $res=mysqli_query($conn,"SELECT id FROM grupe WHERE turniri_id='$id_turnira'");
                    $row=mysqli_fetch_array($res);
                    $id_grupe = $row['id'];
                }
                
                $query = "SELECT * FROM ekipe WHERE turniri_id = '$id_turnira' AND grupe_id ='$id_grupe' ORDER BY bod DESC, gr DESC";
                $result=mysqli_query($conn,$query);
                
                while($row=mysqli_fetch_array($result)):
            ?>
        <tr>
            <td scope="row"><?php echo $row["ime"]?></td>
            <td><?php echo $row["bod"]?></td>
            <td><?php echo $row["pob"]?></td>
            <td><?php echo $row["ner"]?></td>
            <td><?php echo $row["izg"]?></td>
            <td><?php echo $row["zg"]?></td>
            <td><?php echo $row["pg"]?></td>
            <td><?php echo $row["gr"]?></td>
        </tr>

        <?php endwhile;?>
        </tbody>
        </table>
    </div>
    <?php endif; ?>
    <?php 
        $id_turnira = $_GET["id"];

        $sql_korisnik=mysqli_query($conn,"SELECT korisnik_id FROM turniri WHERE id='$id_turnira'");
        $row_korisnik=mysqli_fetch_array($sql_korisnik);
        $korisnik_id = $row_korisnik['korisnik_id'];
        
        if(isset($_GET['id_grupe'])){
            $id_grupe = $_GET['id_grupe'];
        }
        
        if(empty($id_grupe)){
            $res=mysqli_query($conn,"SELECT id FROM grupe WHERE turniri_id='$id_turnira'");
            $row=mysqli_fetch_array($res);
            $id_grupe = $row['id'];
        }
        if(isset($_SESSION['uloga']) && $_SESSION['uloga'] == "admin" && $_SESSION['id'] == $korisnik_id ):?>
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-6">
                    <a style="border-radius: 10px;" class="btn btn-secondary" href="dodajEkipu.php?id=<?php echo $id_turnira;?>&id_grupe=<?php echo $id_grupe;?>">Dodaj ekipu u grupu</a>
                </div>
                <div class="col-6">
                    <button style="float:right; border-radius: 10px;" class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal">Dodaj grupu</button>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="container">
            <div class="row">
                <div class="col">
                    <a style="margin-top: 1rem; border-radius: 10px;" class="btn btn-secondary" href="zavrsnica.php?id=<?php echo $id_turnira;?>">Zavrsnica</a>
                </div>
            </div>
        </div>
        
        


    <!-- Modal - dodaj grupu -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <!-- HEADER -->
        <div class="modal-header">
            <h3 style="color:black;" class="modal-title" id="exampleModalLabel">Dodaj novu grupu</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <!-- BODY -->
        <div class="modal-body">
        <div class="container">
            <form name="dodaj_grupu" class="form-signin" action="" method="POST">
                <div class="input-group form-group">
                    <input type="text" name="ime" class="form-control" placeholder="Unesite ime grupe" autofocus required>
                </div>
                
                <div class="input-group form-group">
                    <input type="number" min="1" name="broj_ekipa" class="form-control" placeholder="Broj ekipa u grupi" required>
                </div>

                <div class="form-group">
                    <input type="submit" name="dodaj_grupu" value="Dodaj grupu" class="btn-register btn-block">
                </div>
            </form>
        </div>
        </div>

    </div>
    </div>
</div>

<script src="main.js"></script>
</body>
</html>