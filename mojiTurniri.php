<?php
    require_once("spoj.php");
?>

<!DOCTYPE html>
<html>
<head>
    <title>FUTSAL TURNIRI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/moj.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body style="background-color: rgba(240, 240, 240, 0.8);">
<?php
    session_start();
    include_once "navbar.php";
?>

<div class="container-fluid table-responsive" style="margin-top: 5rem;">
    <h1>Popis vlastitih turnira</h1>
    
    <table id="editable_table" class="table table-striped table-dark">
    <thead>
        <tr> 
            <th scope="col">Ime</th>
            <th scope="col">Tip</th>
            <th scope="col">Kotizacija</th>
            <th scope="col">Datum početka</th>
            <th scope="col">Lokacija</th>
            <th scope="col">Nagradni fond</th>
            <th scope="col">Broj ekipa</th>
        </tr>
    </thead>
    <tbody>
    <?php   
            $user_id = $_SESSION['id'];
            $query = "SELECT t.*, (SELECT count(turniri_id) FROM ekipe e WHERE e.turniri_id = t.id) count FROM turniri t WHERE korisnik_id = '$user_id'";
            $result=mysqli_query($conn,$query);
            
            while($row=mysqli_fetch_array($result)):
        ?>
        <tr>
            <td scope="row"><?php echo $row["ime"];?></td>
            <td><?php echo $row["tip"];?></td>
            <td><?php echo $row["kotizacija"];?> Kn</td>
            <td><?php echo $row["datum_pocetka"];?></td>
            <td><?php echo $row["lokacija"];?></td>
            <td><?php echo $row["nagradniFond"];?> Kn</td>
            <td>
                <?php
                    echo $row[8];
                ?>
            </td>
            <?php if(isset($_SESSION['k_ime'])):?>
            <td>
                <a style="color:white; float:right;" href="poredak.php?id=<?php echo $row["id"]; ?>" class="btn btn-secondary">Kreiraj strukturu turnira</a>
            </td>
            
            <td>
                <a class="btn btn-secondary" onclick="return confirm('Jeste li sigurni da želite obrisati turnir?')" href="delete.php?id=<?php echo $row["id"]; ?>">
                    <i class="fas fa-ban"></i>
                </a>
            </td>
            
            <?php endif; ?>
        </tr>
        <?php endwhile;?>
    </tbody>
    </table>
</div>


</body>
</html>