<?php
    require_once("spoj.php");
    session_start();
    $error = "";
    
    if(isset($_POST['registracija'])){
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        
        $name = $_POST['ime_prezime'];
        $name_lastname = split_name($name);
        $firstName = $name_lastname[0];
        $lastName = $name_lastname[1];
        $username = $_POST['k_ime'];
        $email = $_POST['email'];
        $password = $_POST['lozinka'];
        $password = md5($password);
		$role = $_POST['uloga'];

        $sql = "SELECT * FROM korisnici WHERE k_ime = '$username'";
        $result = $conn->query($sql);
        if (mysqli_num_rows($result) > 0){
            $error = "Korisničko ime je već zauzeto.";
        }else{
            $sql = "INSERT INTO korisnici (ime, prezime, email, k_ime, lozinka, uloga) VALUES ('$firstName', '$lastName', '$email', '$username', 
            '$password', '$role')";
            if ($conn->query($sql) == TRUE){	
                    echo "<script type='text/javascript'>alert('Uspješno ste se registrirali!');</script>";
                    header("Location: prijava.php");
				}else{
				echo $conn->error;
			}
        }
    }  
  } 

    function split_name($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        return array($first_name, $last_name);
    }

?>

<?php
    if(isset($_SESSION['k_ime'])&&!empty($_SESSION['k_ime'])){
    header("Location: index.php");
    exit();
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>FUTSAL TURNIRI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/moj.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- Bootstrap fontawesome icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body style="background-color: rgba(240, 240, 240, 0.8);">

<?php include_once("navbar.php") ?>

<div class="container">
    <div class="d-flex justify-content-center">
        <div class="card">
            <div class="card-header" style="background-color: white;">
                <h3>REGISTRACIJA</h3>
            </div>
            <div class="card-body">
                <form name="prijava" class="form-signin" action="" method="POST">
                    <div class="input-group form-group">
                        <i class="fas fa-signature icon"></i>
                        <input type="text" name="ime_prezime" class="form-control" placeholder="Unesite vaše ime i prezime" autofocus required>
                    </div>
                    <div class="input-group form-group">
                        <i class="fas fa-user icon"></i>
                        <input type="text" name="k_ime" class="form-control" placeholder="Unesite korisničko ime" required>
                    </div>
                    <div class="input-group form-group">
                        <i class="fas fa-envelope icon"></i>
                        <input type="text" name="email" class="form-control" placeholder="Unesite vašu e-mail adresu" required>
                    </div>
                    <div class="input-group form-group">
                            <i class="fas fa-lock icon"></i>
                        <input type="password" name="lozinka" class="form-control" placeholder="Unesite lozinku" required>
                    </div>
                    <div class="input-group form-group">
                        <i class="fas fa-clipboard-list icon"></i>
                        <select name="uloga" class="form-control" required>
                            <option value="" disabled selected>Što želiš učiniti?</option>
                            <option value="admin">Organizirati turnir</option>
                            <option value="korisnik">Prijaviti se na turnir</option>
                        </select>
                    </div>
                    
                    <div> 
                        <?php if($error != "") {
                            echo "<p>$error</p>";
                            }
                        ?>  
                    </div>

                    <div class="form-group">
                        <input type="submit" name="registracija" value="Registriraj me" class="btn-register btn-block">
                    </div>
                </form>
            </div>
            <a id="a-footer" class="" href="prijava.php">
            <div class="card-footer">
                <p>Već imam korisnički račun</p>
            </div>
            </a>
        </div>
    </div>
</div>

</body>
</html>