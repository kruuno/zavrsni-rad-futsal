<?php
    require_once("spoj.php");
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    $error = "";
    
    if(isset($_POST['dodaj_grupu'])){
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $id_turnira = $_GET["id"];
        $ime = $_POST['ime'];
        $broj_ekipa = $_POST['broj_ekipa'];

        $sql = "SELECT * FROM grupe WHERE ime = '$ime' AND turniri_id ='$id_turnira'";
        $result = $conn->query($sql);

        if (mysqli_num_rows($result) > 0){
            $error = "Ime grupe vec postoji.";
        }else{
            $sql = "INSERT INTO grupe (ime, turniri_id, tip, broj_ekipa) VALUES ('$ime', '$id_turnira', 'playoff', '$broj_ekipa')";
            if ($conn->query($sql) == TRUE){	
                    echo "<script type='text/javascript'>alert('Uspješno ste kreirali play-off!');</script>";
				}else{
				echo $conn->error;
			}
        }
    }  
} 
?>

<!DOCTYPE html>
<html>
<head>
    <title>FUTSAL TURNIRI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/moj.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="jquery.tabledit.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body style="background-color: rgba(240, 240, 240, 0.8);">
<?php
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    include_once "navbar.php";
?>

<div style="margin-top: 5rem;" class="container">
    <div align="center" class="list-group list-group-horizontal">
        <?php
            $id_turnira = $_GET["id"];
            $result = $conn->query("SELECT * FROM grupe WHERE turniri_id = '$id_turnira' AND tip = 'playoff'");
            $row=mysqli_fetch_array($result);
            if (mysqli_num_rows($result) > 0){
                if($row['broj_ekipa'] == 2){
                    echo "<a href='finale.php?id=$id_turnira' class='list-group-item list-group-item-action list-group-item-secondary'>Finale</a>";
                    header('Location: finale.php?id='.$id_turnira);
                } else if ($row['broj_ekipa'] == 4){
                    echo "<a href='polufinale.php?id=$id_turnira' class='list-group-item list-group-item-action list-group-item-secondary'>Polufinale</a>";
                    echo "<a href='finale.php?id=$id_turnira' class='list-group-item list-group-item-action'>Finale</a>";
                    header('Location: polufinale.php?id='.$id_turnira);
                } else if ($row['broj_ekipa'] == 8){
                    echo "<a href='cetvrtfinale.php?id=$id_turnira' class='list-group-item list-group-item-action list-group-item-secondary'>Četvrtfinale</a>";
                    echo "<a href='polufinale.php?id=$id_turnira' class='list-group-item list-group-item-action '>Polufinale</a>";
                    echo "<a href='finale.php?id=$id_turnira' class='list-group-item list-group-item-action '>Finale</a>";
                    header('Location: cetvrtfinale.php?id='.$id_turnira);
                }else if ($row['broj_ekipa'] == 16){
                    echo "<a href='osminaFinala.php?id=$id_turnira' class='list-group-item list-group-item-action list-group-item-secondary'>Osmina finala</a>";
                    echo "<a href='cetvrtfinale.php?id=$id_turnira' class='list-group-item list-group-item-action'>Četvrtfinale</a>";
                    echo "<a href='polufinale.php?id=$id_turnira' class='list-group-item list-group-item-action'>Polufinale</a>";
                    echo "<a href='finale.php?id=$id_turnira' class='list-group-item list-group-item-action '>Finale</a>";
                    header('Location: osminaFinala.php?id='.$id_turnira);
                }
            }
            ?>
    </div>
</div>

<?php 
    $sql2 = "SELECT * FROM turniri WHERE id ='$id_turnira'";
    $result2 = $conn->query($sql2);
    $row=mysqli_fetch_array($result2);
    $korisnik_id = $row['korisnik_id'];
    if(isset($_SESSION['uloga']) && $_SESSION['uloga'] == "admin" && $_SESSION['id'] == $korisnik_id): ?>

<div class="container">
    <div class="d-flex justify-content-center">
        <div class="card">
            <div class="card-header" style="background-color: white;">
                <h3>Kreiraj završnicu</h3>
            </div>
            <div class="card-body">
                <form name="dodaj_grupu" class="form-signin" action="" method="POST">
                    <div class="input-group form-group">
                        <input type="text" name="ime" class="form-control" placeholder="Naziv završnice (pr. Playoff)" autofocus required>
                    </div>

                    <div class="input-group form-group">
                        <select name="broj_ekipa" class="form-control" required>
                            <option value="" disabled selected>Odaberi broj ekipa</option>
                            <option value="2">Finale (2)</option>
                            <option value="4">Polufinale (4)</option>
                            <option value="8">Četvrtfinale (8)</option>
                            <option value="16">Osmina finala (16)</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="submit" name="dodaj_grupu" value="Dodaj završnicu" class="btn-register btn-block">
                    </div>
                </form>
            </div>
            <a id="a-footer" class="" href="poredak.php?id=<?php echo $id_turnira;?>">
            <div class="card-footer">
                <p>Povratak na pregled poretka.</p>
            </div>
            </a>
        </div>
    </div>
</div>
<?php else: ?>
<div style="text-align: center;"> Trenutno nije kreirana završnica! </div>
<?php endif ?>

<script src="main.js"></script>
</body>
</html>