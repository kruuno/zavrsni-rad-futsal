<?php
    require_once("spoj.php");
    session_start();
    $error = "";
    
    if(isset($_POST['prijava'])){
        //Username and password sent from form
        
        $k_ime = $_POST['k_ime'];
        $lozinka = md5($_POST['lozinka']);
        
        //Dohvati sve iz tablice korisnici koji se podudaraju s poslanom formom
        $query = "SELECT * FROM korisnici WHERE k_ime = '$k_ime' AND lozinka = '$lozinka'";

        $result = mysqli_query($conn, $query);

        if(mysqli_num_rows($result) == 1) {
            $row = mysqli_fetch_assoc($result);
            $_SESSION['id'] = $row['id'];
            $_SESSION['k_ime'] = $k_ime;
            $_SESSION['ime'] = $row['ime'];
            $_SESSION['prezime'] = $row['prezime'];
            $_SESSION['uloga'] = $row['uloga'];
            header("Location: index.php");
        } else {
                session_unset();
                session_destroy();
                $error = "Pogrešno korisničko ime ili lozinka!";
        }
    }
    
    mysqli_close($conn);
?>
<?php
    if(isset($_SESSION['k_ime'])&&!empty($_SESSION['k_ime'])){
        header("Location: index.php");
        exit();
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>FUTSAL TURNIRI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/moj.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- Bootstrap fontawesome icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body style="background-color: rgba(240, 240, 240, 0.8);">

<?php include_once("navbar.php") ?>

<div class="container">
    <div class="d-flex justify-content-center">
        <div class="card">
            <div class="card-header" style="background-color: white;">
                <h3>PRIJAVA</h3>
                
            </div>
            <div class="card-body">
                <form name="prijava" class="form-signin" action="" method="POST">
                    <div class="input-group form-group">
                        <i class="fas fa-user icon"></i>
                        <input type="text" name="k_ime" class="form-control" placeholder="Unesite korisničko ime" autofocus required>
                    </div>
            
                    <div class="input-group form-group">
                            <i class="fas fa-lock icon"></i>
                        <input type="password" name="lozinka" class="form-control" placeholder="Unesite lozinku" required>
                    </div>
                    
                    <div> 
                        <?php if($error != "") {
                            echo "<p>$error</p>";
                            }
                        ?>  
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" name="prijava" value="Prijavi me" class="btn-register btn-block">
                    </div>
                </form>
            </div>
            <a id="a-footer" class="" href="registracija.php">
            <div class="card-footer">
                <p>Napravi novi korisnički račun</p>
            </div>
            </a>
        </div>
    </div>
</div>

</body>
</html>