<?php
    require_once("spoj.php");
    $error = "";
    
    if(isset($_POST['prijava_ekipe'])){
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $ime = $_POST['ime'];
        $id = $_GET['id'];

        $sql = "SELECT * FROM ekipe WHERE ime = '$ime' AND turniri_id = '$id'";
        $result = $conn->query($sql);

        $select_id = $conn->query("SELECT korisnik_id FROM turniri WHERE id = '$id'");
        $row=mysqli_fetch_array($select_id);
        $korisnik_id = $row[0];

        $select_korisnik = $conn->query("SELECT email FROM korisnici WHERE id='$korisnik_id'");
        $getEmail = mysqli_fetch_array($select_korisnik);
        
        $to = $getEmail[0];
        $email_subject = "Nova prijava ekipe";
        $msg = "Obavijest o novoj prijavi ekipe na vaš turnir. Ime nove prijavljene ekipe je: $ime \n\n Posjetite web stranicu: https://futsaltour.000webhostapp.com";
        
        if (mysqli_num_rows($result) > 0){
            $error = "Ime ekipe je zauzeto.";
        }else{
            $sql = "INSERT INTO ekipe (turniri_id, grupe_id, ime) VALUES ('$id',0,'$ime')";
            if ($conn->query($sql) == TRUE){
                    mail($to,$email_subject,$msg);
                    echo "<script type='text/javascript'>alert('Uspješno ste prijavili ekipu!');</script>";
				}else{
				echo $conn->error;
			}
        }
    }  
    
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>FUTSAL TURNIRI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/moj.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body style="background-color: rgba(240, 240, 240, 0.8);">
<?php
    session_start();
    include_once "navbar.php";
?>


<div class="container-fluid">
    <div class="d-flex justify-content-center">
        <div class="card">
            <div class="card-header" style="background-color: white;">
                <h3>Prijavi svoju ekipu na turnir</h3>
                <h4><?php 
                    $id = $_GET['id'];
                    $ime_turnira = $conn->query("SELECT ime FROM turniri WHERE id = '$id'");
                    $row_turniri = mysqli_fetch_assoc($ime_turnira);
                    echo $row_turniri["ime"];
                ?></h4>
            </div>
            <div class="card-body">
                <form name="prijava_ekipe" class="form-signin" action="" method="POST">
                    <div class="input-group form-group">
                        <input type="text" name="ime" class="form-control" placeholder="Unesite ime ekipe" autofocus required>
                    </div>

                    <div> 
                        <?php if($error != "") {
                            echo "<p>$error</p>";
                            }
                        ?>  
                    </div>

                    <div class="form-group">
                        <input onclick="return confirm('Potvrdite prijavu')" type="submit" name="prijava_ekipe" value="Prijavi Ekipu" class="btn-register btn-block">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="main.js"></script>
</body>
</html>
