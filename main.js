// Postavit klasu active na odabrani link
$(document).ready(function () {
    var url = window.location;
    $('ul.nav a[href="'+ url +'"]').parent().addClass('active');
    $('ul.nav a').filter(function() {
         return this.href == url;
    }).parent().addClass('active');
});


//Postavit da se mogu odabrati datumi od danasnjeg pa nadalje
var today = new Date().toISOString().split('T')[0];
document.getElementsByName("datumPocetka")[0].setAttribute('min', today);
