# Web aplikacija za upravljanje malonogometnim turnirima

## Autor: Krunoslav Kovač

### Kratki opis aplikacije
- Jednostavna web aplikacija koja služi za upravljanje malonogometnim turnirima. 
- Omogućuje pregled turnira koje kreiraju registrirani korisnici koji žele biti organizatori turnira. 
- Aplikacija omogućuje pračenje rezultata i poretka.